<!DOCTYPE html>
<html>
<head>
	<title>Find Free Chair System</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>

<section class="top_section_about">
	<div class="container">
		<div class="row">
			<div class="col-md-12 header_btn">
			<!-- Button trigger modal -->
			<button type="button" class="login_button hvr-underline-from-left" data-toggle="modal" data-target="#exampleModal">
			  Login
			</button><br>

			<button type="button" class="signin_button hvr-underline-from-left" data-toggle="modal" data-target="#exampleModal1">
			  Register
			</button>

			</div>
		</div>
	</div>
</section>


<section class="chair_details">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-12">						
		    <select class="form-control" id="exampleSelect1">
		      <option value="" disabled="" selected="">Select Your Location</option>
		      <option value="">Floor 0</option>
		      <option value="">Floor 1</option>
		      <option value="">Floor 2</option>
		      <option value="">Floor 3</option>
		    </select>
			</div>
			<div class="col-md-6 col-12">
				<div class="details_class_card">
					<table style="width:100%">
					  <tr>
					    <th><p>Class 1 - Floor 1</p></th>
					    <th><a href="#" data-toggle="modal" data-target="#exampleModal2"><p class="hvr-underline-from-left">View Details:<p></th>
					  </tr>
					  <tr>
					    <td><p>Free Chairs:</p></td>
					    <td><p>20</p></td>
					  </tr>
					  <tr>
					    <td><p>Last Report From:</p></td>
					    <td><p>Leandrit Ferizi</p></td>
					  </tr>
					</table>
					<div class="col-md-12 col-12">
						<div class="row">
							<button class="btn hvr-underline-from-left">Take Chair</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>



<!-- Login and Signup popup -->

<!-- Modal Login-->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content login_modal">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Login</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<input type="" name="" placeholder="Email">
	      	<input type="" name="" placeholder="Password">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="submit_btn hvr-underline-from-left">Submit</button>
	      </div>
	    </div>
	  </div>
	</div>

<!-- Modal To Register Accound -->
	<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content login_modal">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Register</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<input type="" name="" placeholder="Email">
	      	<input type="" name="" placeholder="Password">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="submit_btn hvr-underline-from-left">Submit</button>
	      </div>
	    </div>
	  </div>
	</div>


	<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content details_popup">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Class 1 - Floor 1</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">	 
	      		<input type="" name="" placeholder="Report Currect Number of Chairs">       	
				<button class="btn hvr-underline-from-left">Take Chair</button>

				<div class="more_details_body">
					<h2>Class History:</h2>
					<h4>example@ubt-uni.net</h4>
					<p>Lorem Ipsum is simply dummy text of the printing and</p>
				</div>
				<div class="more_details_body">
					<h2>Class History:</h2>
					<h4>example@ubt-uni.net</h4>
					<p>Lorem Ipsum is simply dummy text of the printing and</p>
				</div>
				<div class="more_details_body">
					<h2>Class History:</h2>
					<h4>example@ubt-uni.net</h4>
					<p>Lorem Ipsum is simply dummy text of the printing and</p>
				</div>
	      </div>
	      <div class="modal-footer">
	      </div>
	    </div>
	  </div>
	</div>



<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>